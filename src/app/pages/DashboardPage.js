import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Notice,
} from "../../_metronic/_partials/controls";

export function DashboardPage() {
  return (
    <>
      <Notice svg="/media/svg/icons/Devices/Display1.svg">
        <h3>
          Admin Panel
        </h3>
        <p>
          Selamat Datang di Aplikasi ASAP - DJP
          <br/>
          Kanwil Riau
        </p>
      </Notice>
      <Card>
        {/* <CardHeader
          title="DASHBOARD"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader> */}
        <CardBody>
          <div className="text-center">
          <img alt="logo" src="/media/logos/user_asap.png" style={{"width": "30%"}}/>
            <h2>Selamat Datang di Sistem ASAP</h2>
            <span>Aplikasi ASAP Versi 1.0.1</span>
          </div>
        </CardBody>
      </Card>
    </>
  );
}
