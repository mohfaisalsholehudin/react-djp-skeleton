import React from "react";

export function StockColumnFormatterMasterAtk(cellContent, row) {

    const getLabelCssClasses = () => {
        return `label label-lg label-light-success label-inline`;
   
      
    };
    return (
      <span className={getLabelCssClasses()}>{row.stock}</span>
    );
  }
  
  