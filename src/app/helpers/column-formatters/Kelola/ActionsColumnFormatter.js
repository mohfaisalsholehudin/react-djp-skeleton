// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
export function ActionsColumnFormatterKelolaUsulan(
    cellContent,
    row,
    rowIndex,
    { openEditDialog, openDetailDialog, openModal }
  ) {
    return (
      <>
        <a
        title="Lihat Detail Usulan"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3 my-3"
        onClick={() => openModal(row.id)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
      </>
    );
  }
  export function ActionsColumnFormatterKelolaPinjam(
    cellContent,
    row,
    rowIndex,
    { openEditDialog, openDetailDialog, openModal }
  ) {
    return (
      <>
        <a
        title="Lihat Detail Pinjaman"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3 my-3"
        onClick={() => openModal(row.id)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
      </>
    );
  }

  export function ActionsColumnFormatterKelolaKembali(
    cellContent,
    row,
    rowIndex,
    { openReturnDialog, openDetailDialog }
  ) {
    const checkStatus = (status) => {
      switch (status) {
        case "Belum Kembali":
          return (
            <>
              <a
                title="Lihat Detail Pengembalian"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => openReturnDialog(row.id)}
              >
                <span className="svg-icon svg-icon-md svg-icon-primary">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-edit text-primary"></i>
                </span>
              </a>
            </>
          );
        case "Terlambat Kembali":
          return (
            <>
              <a
                title="Lihat Detail Pengembalian"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => openReturnDialog(row.id)}
              >
                <span className="svg-icon svg-icon-md svg-icon-primary">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-edit text-primary"></i>
                </span>
              </a>
            </>
          );
  
        case "Sudah Kembali":
          return (
            <>
              <a
                title="Lihat Detail Pengembalian"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => openDetailDialog(row.id)}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );
  
        default:
          break;
      }
    };





  return <>{checkStatus(row.status)}</>;

    // return (
    //   <>
    //     <a
    //     title="Lihat Detail Pinjaman"
    //     className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3 my-3"
    //     onClick={() => openModal(row.id)}
    //   >
    //     <span className="svg-icon svg-icon-md svg-icon-dark">
    //       <i className="fas fa-eye text-dark"></i>
    //     </span>
    //   </a>
    //   </>
    // );
  }


  export function ActionsColumnFormatterKelolaKembaliDetail(
    cellContent,
    row,
    rowIndex,
    { openReturnDialog }
  ) {
    return (
      <>
  
        {/* <a
          title="Kembalikan"
          className="btn btn-icon btn-light btn-hover-danger btn-sm"
          onClick={() => openReturnDialog(row.id)}
        >
          <span className="svg-icon svg-icon-md svg-icon-primary">
            <i className="fas fa-reply text-primary"></i>
          </span>
        </a> */}
         <button
                type="button"
                className="btn btn-success ml-2"
                onClick={() => openReturnDialog(row.id)}
                // style={{
                //   boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                //   // paddingRight: "0px", paddingLeft: "0px"
                // }}
                // disabled={disabled}
              >
                <i className="fas fa-shopping-cart"></i>
                Kembalikan
              </button>
      </>
    );
  }