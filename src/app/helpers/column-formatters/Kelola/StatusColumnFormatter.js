import React from "react";

export function StatusColumnFormatterKelolaUsulan(cellContent, row) {
  const CustomerStatusCssClasses = ["warning", "success", "primary", "info", ""];
  const CustomerStatusTitles = ["Pengajuan", "Setuju", "Selesai", "Menunggu Persetujuan", ""];
  let status = "";

  switch (row.status) {
    case "Pengajuan":
      status = 0;
      break;

    case "Setuju":
      status = 1;
      break;
    case "Selesai":
      status = 2;
      break;
    case "Menunggu Persetujuan":
      status = 3;
      break;

    default:
      status = 4;
      break;
  }
  const getLabelCssClasses = () => {
    if (row.status === "") {
      return "";
    } else {
      return `label label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <>
      <span className={getLabelCssClasses()}>
        {CustomerStatusTitles[status]}
      </span>
    </>
  );
}


export function StatusColumnFormatterKelolaPinjam(cellContent, row) {
  const CustomerStatusCssClasses = ["primary", "warning", "danger", ""];
  const CustomerStatusTitles = ["Sudah Kembali", "Terlambat Kembali", "Belum Kembali", ""];
  let status = "";

  switch (row.status) {
    case "Sudah Kembali":
      status = 0;
      break;

    case "Terlambat Kembali":
      status = 1;
      break;
    case "Belum Kembali":
      status = 2;
      break;

    default:
      status = 3;
      break;
  }
  const getLabelCssClasses = () => {
    if (row.status === "") {
      return "";
    } else {
      return `label label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <>
      <span className={getLabelCssClasses()}>
        {CustomerStatusTitles[status]}
      </span>
    </>
  );
}


export function StatusColumnFormatterKelolaKembali(cellContent, row) {
  const CustomerStatusCssClasses = ["primary", "warning", "danger", ""];
  const CustomerStatusTitles = ["Sudah Kembali", "Terlambat Kembali", "Belum Kembali", ""];
  let status = "";

  switch (row.status) {
    case "Sudah Kembali":
      status = 0;
      break;

    case "Terlambat Kembali":
      status = 1;
      break;
    case "Belum Kembali":
      status = 2;
      break;

    default:
      status = 3;
      break;
  }
  const getLabelCssClasses = () => {
    if (row.status === "") {
      return "";
    } else {
      return `label label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <>
      <span className={getLabelCssClasses()}>
        {CustomerStatusTitles[status]}
      </span>
    </>
  );
}
