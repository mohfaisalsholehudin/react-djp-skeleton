import React, { useState } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, InputAtk, Select as Sel, Textarea } from "../../../helpers";
import { CustomUploadPhoto } from "../../../helpers/form/CustomUploadPhoto";
import { generateCode } from "../../references/Api";

function AtkForm({ content, btnRef, saveForm }) {
  const [show, setShow] = useState(false);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    namaAtk: Yup.string().required("Nama ATK wajib diisi"),
    kode: Yup.string().required("Kode ATK wajib diisi"),
    deskripsi: Yup.string().required("Deskripsi wajib diisi"),
    file: Yup.mixed()
      .required("File wajib diisi")
      .test(
        "fileSize",
        "File terlalu besar",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Format file tidak sesuai",
        (value) =>
          value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))
      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["image/jpeg"];

  // useEffect(() => {
  //   content.status !== "" ? setShow(true):setShow(false)
  //  }, [content]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
          // console.log(values)
        }}
      >
        {({ handleSubmit, setFieldValue }) => {
          const handleClick = () => {
            if (show) {
              setFieldValue("kode", "");
              setShow(!show);
            } else {
              generateCode().then(({ data }) => {
                setFieldValue("kode", data);
                setShow(!show)
              });
            }
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* Field Kode ATK */}
                <div
                  className="form-group row"
                  // style={{ marginBottom: "0.5rem" }}
                >
                  {show ? (
                    <>
                      <Field
                        name="kode"
                        component={InputAtk}
                        placeholder="Kode ATK"
                        label="Kode ATK"
                        disabled
                      />
                      <div className="col-lg-3 col-xl-3">
                        <button
                          type="button"
                          className="btn btn-light ml-2"
                          onClick={() => handleClick()}
                        >
                          <i className="fas fa-times"></i>
                          Hapus Kode
                        </button>
                      </div>
                    </>
                  ) : (
                    <>
                      <Field
                        name="kode"
                        component={InputAtk}
                        placeholder="Kode ATK"
                        label="Kode ATK"
                      />
                      <div className="col-lg-3 col-xl-3">
                        <button
                          type="button"
                          className="btn btn-primary ml-2"
                          onClick={() => handleClick()}
                        >
                          <i className="fas fa-barcode"></i>
                          Kode Otomatis
                        </button>
                      </div>
                    </>
                  )}
                </div>

                {/* Field Nama ATK */}
                <div className="form-group row">
                  <Field
                    name="namaAtk"
                    component={Input}
                    placeholder="Nama ATK"
                    label="Nama ATK"
                  />
                </div>
                {/* Field Deskripsi */}
                <div className="form-group row">
                  <Field
                    name="deskripsi"
                    component={Textarea}
                    placeholder="Deskripsi"
                    label="Deskripsi"
                  />
                </div>
                {/* Field Unggah Foto */}

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unggah Foto
                  </label>
                  <div className="col-lg-9 col-xl-9">
                    <Field
                      name="file"
                      component={CustomUploadPhoto}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default AtkForm;
