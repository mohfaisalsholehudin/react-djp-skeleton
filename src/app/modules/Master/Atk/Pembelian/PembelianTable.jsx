/* Library */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";

/* Utility */

function PembelianTable({ tab = "proses" }) {
  const history = useHistory();
  //   const [content, setContent] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);

  const content = [
    {
      id: "1",
      kode_item: "1100111",
      nama: "Spidol White Board",
      stock: "4",
      photo: "/media/users/user.png",
    },
    {
      id: "2",
      kode_item: "1100222",
      nama: "Drum Cartridge Docu",
      stock: "50",
      photo: "/media/users/user.png",
    },
    {
      id: "3",
      kode_item: "1100333",
      nama: "Pensil Hitam",
      stock: "100",
      photo: "/media/users/user.png",
    },
  ];

  const pembelian = () => history.push("/master/atk/pembelian");
  //   const edit = (id_usulan_matrix) =>
  //     history.push(
  //       `/matrix/usulan-matrix/${id_usulan_matrix}/edit`
  //     );
  const deleteAction = (id) => {
    // swal({
    //   title: "Apakah Anda Yakin?",
    //   text: "Klik OK untuk melanjutkan",
    //   icon: "warning",
    //   buttons: ["Batal", "Ok"],
    //   dangerMode: true,
    // }).then((willDelete) => {
    //   if (willDelete) {
    //     deleteMatrixUsulan(id).then(({ status }) => {
    //       if (status === 200) {
    //         swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
    //           history.push("/dashboard");
    //           history.replace("/matrix/usulan-matrix");
    //         });
    //       } else {
    //         swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //           history.push("/dashboard");
    //           history.replace("/matrix/usulan-matrix");
    //         });
    //       }
    //     });
    //   }
    //   else {
    //     swal("Your imaginary file is safe!");
    //   }
    // });
  };

  const apply = (id_matrix) => {
    // swal({
    //   title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
    //   text: "Klik OK untuk melanjutkan",
    //   icon: "warning",
    //   buttons: ["Batal", "Ok"],
    //   dangerMode: false,
    // }).then((willApply) => {
    //   if (willApply) {
    //     updateStatusMatrix(id_matrix,"","Review Pengajuan Matrix").then(({ status }) => {
    //       if (status === 201 || status === 200) {
    //         // updateWaktuPublish(id_usulan_template, getCurrentDate());
    //         swal("Berhasil", "Usulan berhasil diajukan", "success").then(
    //           () => {
    //             history.push("/dashboard");
    //             history.replace("/matrix/usulan-matrix");
    //           }
    //         );
    //       } else {
    //         swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
    //           history.push("/dashboard");
    //           history.replace("/matrix/usulan-matrix");
    //         });
    //       }
    //     });
    //   }
    // });
  };

  useEffect(() => {
    // switch (tab) {
    //   case "proses":
    //     getMatrixUsulan().then(({data})=> {
    //       setLoading(false)
    //       data.map(dt => {
    //         return dt.status !== "Terima Pengajuan Matrix" && dt.status !== "Tolak Pengajuan Matrix" ? setContent(content => [...content, dt]): null
    //       })
    //     })
    //     break;
    //   case "selesai":
    //     getMatrixUsulan().then(({data})=> {
    //       setLoading(false)
    //       data.map(dt => {
    //         return dt.status === "Terima Pengajuan Matrix" ? setContent(content => [...content, dt]): null
    //       })
    //     })
    //     break;
    //     case "tolak":
    //       getMatrixUsulan().then(({data})=> {
    //         setLoading(false)
    //         data.map(dt => {
    //           return dt.status === "Tolak Pengajuan Matrix" ? setContent(content => [...content, dt]): null
    //         })
    //       })
    //       break;
    //   default:
    //     break;
    // }
  }, [tab]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      hidden: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        width: "100px",
      },
    },
    {
      dataField: "id",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "kode_item",
      text: "Kode Item",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        width: "10px",
      },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "photo",
      text: "Foto",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.PhotoColumnFormatterMasterPengguna,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        width: "50px",
      },
    },
  
    {
      dataField: "nama",
      text: "Nama Item",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        width: "100px",
      },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "stock",
      text: "Stock",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StockColumnFormatterMasterAtk,
      style: {
        width: "80px",
      },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   hidden: tab === 'selesai' || tab === 'tolak' ? true : false,
    //   formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeUsulanMatrix,
    // },
    // {
    //   dataField: "action",
    //   text: "Aksi",
    //   formatter:
    //     columnFormatters.ActionsColumnFormatterProcessKnowledgeUsulanMatrix,
    //   formatExtraData: {
    //     openEditDialog: edit,
    //     openDeleteDialog: deleteAction,
    //     publishKnowledge: apply,
    //     showReview: review,
    //   },
    //   classes: "text-center pr-0",
    //   headerClasses: "text-center pr-3",
    //   style: {
    //     minWidth: "100px",
    //   },
    // },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  // const emptyDataMessage = () => {
  //   return "Tidak Ada Data";
  // };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                            placeholder="Cari"
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-warning"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={pembelian}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Shopping/Cart1.svg"
                                )}
                              />
                            </span>
                            Pembelian Baru
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination
                        paginationProps={paginationProps}
                        //   isLoading={loading}
                      />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default PembelianTable;
