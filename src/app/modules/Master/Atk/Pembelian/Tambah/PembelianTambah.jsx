/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React,{useState} from "react";
import { useHistory } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../../_metronic/_partials/controls";
import { Nav, Tab } from "react-bootstrap";
import PembelianTambahForm from "./PembelianTambahForm";
import PembelianTambahTable from "./PembelianTambahTable";
import { checkAtkEmpty, getPenggunaById } from "../../../../references/Api";

function PembelianTambah() {
  const history = useHistory();
  const [content, setContent] = useState([])
  const [pengguna, setPengguna] = useState([])

  const backAction = () => {
    history.push("/master/atk/pembelian");
  };

  useState(()=> {
    checkAtkEmpty().then(({data})=> {
      setContent(data[0])
      getPenggunaById(data[0].userId).then(({data})=> {
        setPengguna(data)
      })
    })
  },[])

  const dateFormat = (val) => {
    const date = new Date(val);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
    return tanggal + "-" + bulan + "-" + tahun ;
  }

  return (
    <>
      {/* <Card>
        <CardHeader
          title="ATK Baru"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader> */}
      {/* <CardFooter style={{ borderTop: "none" }}>
          <>
            <div className="col-lg-12" style={{ textAlign: "right" }}>
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveForm}
            disabled={false}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
            </div>
          </>
        </CardFooter> */}
      {/* </Card> */}
      <>
        <div className="d-flex flex-row">
          <div
            className="flex-row-auto offcanvas-mobile w-350px w-xxl-500px"
            id="kt_profile_aside"
          >
            <div className="card card-custom card-stretch">
              {/* <div className="card-header py-3">
                <div className="card-title align-items-start flex-column">
                  <h3 className="card-label font-weight-bolder text-dark">
                    ATK Baru
                  </h3>
                </div>
              </div> */}
              <div className="card-body pt-4">
                <div className="py-5">
                  <div className="d-flex align-items-center justify-content-between mb-2">
                    <h5 className="font-weight-bold">PEMBELIAN</h5>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-2">
                    <span className="font-weight-normal mr-2">No. PO:</span>
                    <span className="text-muted text-hover-primary">{content.nomorPo}</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-2">
                    <span className="font-weight-normal mr-2">Tanggal:</span>
                    <span className="text-muted">{dateFormat(content.tglPembelian)}</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-2">
                    <span className="font-weight-normal mr-2">
                      Dibuat Oleh:
                    </span>
                    <span className="text-muted">{pengguna.name}</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between">
                    <span className="font-weight-normal mr-2">Pada:</span>
                    <span className="text-muted">{dateFormat(content.createdAt)}</span>
                    {/* <span className="text-muted">2023-12-06 14:05:21</span> */}
                  </div>
                </div>
              </div>
              <div className="card-body pt-4">
              <div className="d-flex align-items-center justify-content-between mb-2">
                    <h5 className="font-weight-bold">MASUKKAN ITEM</h5>
                  </div>
              <PembelianTambahForm 
                id={content.id}
              />
              </div>
            </div>
          </div>
          <div className="flex-row-fluid ml-lg-8">
            <PembelianTambahTable
              id={content.id}
            />
          </div>
        </div>
      </>
    </>
  );
}

//#a6c8e6
export default PembelianTambah;
