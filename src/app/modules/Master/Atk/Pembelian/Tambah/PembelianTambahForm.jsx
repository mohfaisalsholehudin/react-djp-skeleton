import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import * as Yup from "yup";
import Select from "react-select";

import { Input, DatePickerField, InputSubtotal } from "../../../../../helpers";
import { getAtk, saveDetailPurchaseAtk } from "../../../../references/Api";

function PembelianTambahForm({ id }) {
  const history = useHistory();

  const [listAtk, setListAtk] = useState([]);
  const [kodeAtk, setKodeAtk] = useState([]);
  const [valNamaAtk, setValNamaAtk] = useState();

  const initValues = {
    kode: "",
    namaAtk: "",
    harga: 0,
    jumlah: 0,
    subtotal: 0,
  };
  const ValidationSchema = Yup.object().shape({
    kode: Yup.string(),
    // .min(2, "Kode ATK 2 symbols"),
    // .required("Kode ATK wajib diisi"),
    namaAtk: Yup.string(),
    // .required("Nama ATK Wajib "),
    harga: Yup.number().required("Harga wajib diisi"),
    jumlah: Yup.number().required("Jumlah wajib diisi"),
  });

  const saveForm = (val) => {
    saveDetailPurchaseAtk(
      id,
      val.id,
      val.jumlah,
      val.harga,
      val.harga * val.jumlah.toString()
    ).then(({ data }) => {
      if (data) {
        history.push("/dashboard");
        history.replace("/master/atk/pembelian/tambah");
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.replace("/master/atk/pembelian/tambah");
        });
      }
    });
  };

  useEffect(() => {
    getAtk().then(({ data }) => {
      // setListAtk(data)
      data.map((data) => {
        setListAtk((listAtk) => [
          ...listAtk,
          {
            label: data.namaAtk,
            value: data.kode,
            harga: data.harga,
            id: data.id,
          },
        ]);
      });
      data.map((data) => {
        setKodeAtk((kodeAtk) => [
          ...kodeAtk,
          {
            value: data.namaAtk,
            label: data.kode,
            // jenis: data.jenis
          },
        ]);
      });
    });
  }, []);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={ValidationSchema}
        onSubmit={(values) => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({ handleSubmit, setFieldValue, values }) => {
          const handleChangeJumlah = (val) => {
            setFieldValue("jumlah", val.target.value);
            // Remove non-digit characters from the input value
            const input = val.target.value.replace(/[^0-9.]/g, "");
            const tot = input * values.harga;

            // Format the input value as a currency string
            const formattedValue = formatCurrency(tot);
            setFieldValue("subtotal", formattedValue);
          };
          const handleChangeHarga = (val) => {
            setFieldValue("harga", val.target.value);

            // Remove non-digit characters from the input value
            const input = val.target.value.replace(/[^0-9.]/g, "");
            const tot = input * values.jumlah;

            // Format the input value as a currency string
            const formattedValue = formatCurrency(tot);
            setFieldValue("subtotal", formattedValue);
          };
          const formatCurrency = (value) => {
            // Check if the input is a valid number
            if (isNaN(value)) {
              return "";
            }

            // Parse the input value to a floating-point number
            const floatValue = parseFloat(value);

            // Format the number as currency (you can adjust this logic based on your requirements)
            const formattedCurrency = floatValue.toLocaleString("id-ID", {
              style: "currency",
              currency: "IDR",
            });

            return formattedCurrency;
          };
          const handleChangeNamaAtk = (val) => {
            setFieldValue("kode", val.value);
            setFieldValue("namaAtk", val.label);
            setFieldValue("harga", val.harga);
            setFieldValue("id", val.id);
            setValNamaAtk(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD KODE ATK */}

                <div
                  className="form-group row"
                  style={{ marginBottom: "0rem" }}
                >
                  <label className="col-xl-2 col-lg-2 col-form-label"></label>
                  <label className="col-xl-5 col-lg-5">Kode ATK</label>
                  <label className="col-xl-5 col-lg-5">Nama ATK</label>
                </div>
                <div
                  className="form-group row"
                  style={{ alignItems: "center" }}
                >
                  <label
                    className="col-xl-2 col-lg-2 col-form-label"
                    style={{ paddingRight: "0px", paddingLeft: "0px" }}
                  >
                    Cari
                  </label>
                  <div
                    className="col-lg-5 col-xl-5"
                    style={{ paddingRight: "0px", paddingLeft: "10px" }}
                  >
                    <Select
                      isDisabled={true}
                      options={kodeAtk}
                      placeholder="Kode"
                      value={kodeAtk.filter(
                        (data) => data.label === values.kode
                      )}
                    />
                  </div>
                  <div
                    className="col-lg-5 col-xl-5"
                    style={{ paddingRight: "0px", paddingLeft: "10px" }}
                  >
                    <Select
                      options={listAtk}
                      placeholder="Nama"
                      onChange={(value) => handleChangeNamaAtk(value)}
                      value={listAtk.filter(
                        (data) => data.label === valNamaAtk
                      )}
                      // value={kodeAtk.filter((data) => data.label === valSurv)}
                    />
                  </div>
                </div>
                {/* FIELD HARGA */}
                <div
                  className="form-group row"
                  style={{ alignItems: "center" }}
                >
                  <div
                    className="col-xl-2 col-lg-2"
                    style={{ paddingRight: "0px", paddingLeft: "0px" }}
                  >
                    Harga
                  </div>
                  <div
                    className="col-lg-10 col-xl-10"
                    style={{ paddingRight: "0px" }}
                  >
                    <input
                      type="number"
                      name="harga"
                      placeholder="Harga"
                      value={values.harga}
                      className="form-control form-control-lg"
                      onChange={(e) => handleChangeHarga(e)}
                      style={{ marginRight: "10px" }}
                    />
                  </div>
                </div>

                {/* FIELD JUMLAH */}
                <div
                  className="form-group row"
                  style={{ alignItems: "center" }}
                >
                  <div
                    className="col-xl-2 col-lg-2"
                    style={{ paddingRight: "0px", paddingLeft: "0px" }}
                  >
                    Jumlah
                  </div>
                  <div
                    className="col-lg-10 col-xl-10"
                    style={{ paddingRight: "0px" }}
                  >
                    <input
                      type="number"
                      name="jumlah"
                      placeholder="Jumlah"
                      className="form-control form-control-lg"
                      onChange={(e) => handleChangeJumlah(e)}
                      style={{ marginRight: "10px" }}
                    />
                  </div>
                </div>

                <div className="form-group row">
                  <Field
                    name="subtotal"
                    component={InputSubtotal}
                    placeholder="Sub Total"
                    label="Sub Total"
                    disabled
                  />
                </div>
                {/* <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button> */}
                <div
                  className="form-group row"
                  style={{ display: "block", textAlign: "right" }}
                >
                  <button
                    type="submit"
                    className="btn btn-success"
                    onSubmit={() => handleSubmit()}
                    disabled={false}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      // paddingRight: "0px", paddingLeft: "0px"
                    }}
                    // disabled={disabled}
                  >
                    <i className="fas fa-plus"></i>
                    Tambah
                  </button>
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PembelianTambahForm;
