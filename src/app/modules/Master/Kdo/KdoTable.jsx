/* eslint-disable react-hooks/exhaustive-deps */
/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { getKdo } from "../../references/Api";

/* Utility */

function KdoTable({ tab = "proses" }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [data, setData] = useState([]);
  const [searchTxt, setSearchTxt] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [sizePage, setSizePage] = useState(10);
  const [loading, setLoading] = useState(true);

   const edit = (id) =>
      history.push(
        `/master/kdo/${id}/edit`
      );
 

  useEffect(() => {
    getKdo(
      currentPage === 0 ? currentPage : currentPage - 1,
      sizePage,
      "id",
      "asc",
      searchTxt ? searchTxt : ""
    ).then(({data})=> {
 
      if (data.content.length > 0) {
        setLoading(false);
        setContent(data.content)
        setData(data)
      } else {
        setLoading(false);
        swal({
          title: "Data Tidak Tersedia!",
          icon: "info",
          closeOnClickOutside: false,
        }).then((willApply) => {
          if (willApply) {
            // history.push("/logout");
          }
        });
      }
    })
  }, [currentPage, sizePage]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      hidden: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage === 0 ?  currentPage + 1 - 1 : currentPage -1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      // style: {
      //   width: "100px",
      // },
    },
    {
      dataField: "id",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "photo",
      text: "Foto",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.PhotoColumnFormatterMasterKdo,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      // style: {
      //   width: "50px",
      // },
    },
    {
      dataField: "jenis",
      text: "Jenis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "tahun",
      text: "Tahun",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
  
    {
      dataField: "nomorPlat",
      text: "No. Plat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "100px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "ready",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterMasterKdo,
      // style: {
      //   width: "80px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   hidden: tab === 'selesai' || tab === 'tolak' ? true : false,
    //   formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeUsulanMatrix,
    // },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterMasterKdo,
      formatExtraData: {
        openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // publishKnowledge: apply,
        // showReview: review,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      // style: {
      //   minWidth: "100px",
      // },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: currentPage === 0 ? currentPage + 1 : currentPage === 1 ? currentPage : currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: data.totalElements,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  // const emptyDataMessage = () => {
  //   return "Tidak Ada Data";
  // };
  const { SearchBar } = Search;
  const handleTableChange = (
    type,
    { sortField, sortOrder, data, filters, ...props }
  ) => {
    if (type === "search") {
      setCurrentPage(0);
      setSearchTxt(props.searchText);
          getKdo(
            0,
            sizePage,
            "",
            "",
            props.searchText
          ).then(({ data }) => {
            if (data.content.length > 0) {
              setContent(data.content);
              setData(data);
            } else {
              swal({
                title: "Data Tidak Tersedia!",
                icon: "info",
                closeOnClickOutside: false,
              }).then((willApply) => {
                if (willApply) {
                  // history.push("/logout");
                }
              });
            }
          });
    }
    let result;
    if (sortOrder === "desc") {
      result = data.sort((a, b) => {
        if (a[sortField] > b[sortField]) {
          return 1;
        } else if (b[sortField] > a[sortField]) {
          return -1;
        }
        return 0;
      });
      setContent(result);
    } else {
      result = data.sort((a, b) => {
        if (a[sortField] > b[sortField]) {
          return -1;
        } else if (b[sortField] > a[sortField]) {
          return 1;
        }
        return 0;
      });
      setContent(result);
    }
  };
  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                            placeholder="Cari"
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        remote
                        onTableChange={handleTableChange}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination
                        paginationProps={paginationProps}
                          isLoading={loading}
                      />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default KdoTable;
