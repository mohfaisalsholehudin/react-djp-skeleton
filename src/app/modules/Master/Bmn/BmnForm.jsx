import React, { useState } from "react";
import { Field, Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Input, Select as Sel, Textarea } from "../../../helpers";
import { CustomUploadPhoto } from "../../../helpers/form/CustomUploadPhoto";

function BmnForm({ content, btnRef, saveForm }) {
  const [thisYear, setThisYear] = useState(new Date().getFullYear());
  const minOffset = 0;
  const maxOffset = 20;
  // const status = [
  //   {value: "Baik", label:"Baik",},
  //   {value: "Rusak Berat", label:"Rusak Berat"}
  // ]



  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    kode: Yup.string()
      .required("Kode BMN wajib diisi"),
    tahun: Yup.string().required("Tahun BMN wajib diisi"),
    tahunPengadaan: Yup.string().required("Tahun Pengadaan wajib diisi"),
    namaBmn: Yup.string()
      .required("Nama BMN wajib diisi"),
      pic: Yup.string()
      .required("Nama Penanggungjawab wajib diisi"),
    deskripsi: Yup.string().required("Deskripsi wajib diisi"),
    status: Yup.string().required("Status wajib diisi"),
    // stock: Yup.number().required("Stok wajib diisi"),
    file: Yup.mixed()
      .required("File wajib diisi")
      .test(
        "fileSize",
        "File terlalu besar",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Format file tidak sesuai",
        (value) => value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))

      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["image/jpeg"];


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({ handleSubmit }) => {
          const options = [];
          for (let i = minOffset; i <= maxOffset; i++) {
            const year = thisYear - i;
            options.push(
              <option key={year} value={year}>
                {year}
              </option>
            );
          }

          const status = [
            <option key={"Baik"} value={"Baik"}>
                {"Baik"}
              </option>,
              <option key={"Rusak Berat"} value={"Rusak Berat"}>
              {"Rusak Berat"}
            </option>
          ]
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Kode */}
                <div
                  className="form-group row"
                >
                  <Field
                    name="kode"
                    component={Input}
                    placeholder="Kode"
                    label="Kode"
                  />
                </div>
                {/* Field Nama BMN */}
                <div
                  className="form-group row"
                >
                  <Field
                    name="namaBmn"
                    component={Input}
                    placeholder="Nama BMN"
                    label="Nama BMN"
                  />
                </div>
                {/* Field Tahun BMN */}
                <div className="form-group row">
                  <Sel name="tahun" label="Tahun BMN">
                    <option>--Pilih Tahun BMN--</option>
                    {options}
                  </Sel>
                </div>
                  {/* Field Tahun Pengadaan */}
                  <div className="form-group row">
                  <Sel name="tahunPengadaan" label="Tahun Pengadaan">
                    <option>--Pilih Tahun Pengadaan--</option>
                    {options}
                  </Sel>
                </div>
                {/* Field Deskripsi */}
                <div className="form-group row">
                  <Field
                    name="deskripsi"
                    component={Textarea}
                    placeholder="Deskripsi"
                    label="Deskripsi"
                  />
                </div>
                 {/* Field Status */}
                 <div className="form-group row">
                  <Sel name="status" label="Status BMN">
                    <option>--Pilih Status BMN--</option>
                    {/* {status} */}
                    {status}
                  </Sel>
                </div>
                 {/* Field Nama Penanggungjawab */}
                 <div
                  className="form-group row"
                >
                  <Field
                    name="pic"
                    component={Input}
                    placeholder="Nama Penanggungjawab"
                    label="Nama Penanggungjawab"
                  />
                </div>
                   {/* Field Stok */}
                   {/* <div className="form-group row">
                  <Field
                    name="stock"
                    component={Input}
                    placeholder="Stok"
                    label="Stok"
                    type="number"
                  />
                </div> */}
                {/* Field Unggah Foto */}

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unggah Foto
                  </label>
                  <div className="col-lg-9 col-xl-9">
                    <Field
                      name="file"
                      component={CustomUploadPhoto}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default BmnForm;
