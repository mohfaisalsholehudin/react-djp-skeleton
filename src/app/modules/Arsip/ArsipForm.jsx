import React, { useState } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, Select as Sel, Textarea } from "../../helpers";
import { CustomUploadDocument } from "../../helpers/form/CustomUploadDocument";

function ArsipForm({ content, btnRef, saveForm }) {
  const [thisYear, setThisYear] = useState(new Date().getFullYear());
  const minOffset = 0;
  const maxOffset = 20;
  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    tipeArsip: Yup.string().required("Tipe Arsip wajib diisi"),
    nomor: Yup.string().required("Nomor Arsip wajib diisi"),
    tahun: Yup.string().required("Tahun wajib diisi"),
    namaArsip: Yup.string().required("Nama Arsip wajib diisi"),
    deskripsi: Yup.string().required("Deskripsi wajib diisi"),
    jumlahLembar: Yup.number().required("Jumlah Lembar wajib diisi"),
    gudang: Yup.string().required("Gudang wajib diisi"),
    lemari: Yup.string().required("Lemari wajib diisi"),
    rak: Yup.string().required("Rak wajib diisi"),
    boks: Yup.string().required("boks wajib diisi"),
    file: Yup.mixed()
      .required("File wajib diisi")
      .test(
        "fileSize",
        "File terlalu besar",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Format file tidak sesuai",
        (value) =>
          value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))
      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({ handleSubmit }) => {
          const options = [];
          for (let i = minOffset; i <= maxOffset; i++) {
            const year = thisYear - i;
            options.push(
              <option key={year} value={year}>
                {year}
              </option>
            );
          }
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Tipe Arsip */}
                <div className="form-group row">
                  <Sel name="tipeArsip" label="Tipe Arsip">
                    <option value="SPT Tahunan">SPT Tahunan</option>
                    <option value="SPT Masa">SPT Masa</option>
                    <option value="Pengukuhan PKP">Pengukuhan PKP</option>
                    <option value="Pemindahbukuan">Pemindahbukuan</option>
                    <option value="Tagihan Pajak">Tagihan Pajak</option>
                    <option value="SPMKP">SPMKP </option>
                    <option value="Surat Ketetapan Pajak">
                      Surat Ketetapan Pajak{" "}
                    </option>
                  </Sel>
                </div>
                {/* Field Nomor Arsip */}
                <div className="form-group row">
                  <Field
                    name="nomor"
                    component={Input}
                    placeholder="Nomor Arsip"
                    label="Nomor Arsip"
                  />
                </div>
                {/* Field Nama Arsip */}
                <div className="form-group row">
                  <Field
                    name="namaArsip"
                    component={Input}
                    placeholder="Nama Arsip"
                    label="Nama Arsip"
                  />
                </div>
                {/* Field Tahun */}
                <div className="form-group row">
                  <Sel name="tahun" label="Tahun">
                    <option>--Pilih Tahun--</option>
                    {options}
                  </Sel>
                </div>
                {/* Field Deskripsi */}
                <div className="form-group row">
                  <Field
                    name="deskripsi"
                    component={Textarea}
                    placeholder="Deskripsi"
                    label="Deskripsi"
                  />
                </div>
                {/* Field Jumlah Lembar */}
                <div className="form-group row">
                  <Field
                    name="jumlahLembar"
                    component={Input}
                    placeholder="Jumlah Lembar"
                    label="Jumlah Lembar"
                    type="number"
                  />
                </div>
                <div className="separator separator-dashed my-5"></div>
                <div className="row">
                  <div className="col-lg-12 col-xl-12">
                    <h5 className="font-weight-bold mb-6">
                      Lokasi Penyimpanan
                    </h5>
                  </div>
                </div>

                {/* Field Gudang */}
                <div className="form-group row">
                  <Sel name="gudang" label="Gudang">
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                  </Sel>
                </div>
                {/* Field Lemari */}
                <div className="form-group row">
                  <Sel name="lemari" label="Lemari">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </Sel>
                </div>
                {/* Field Rak */}
                <div className="form-group row">
                  <Sel name="rak" label="Rak">
                    <option value="I">I</option>
                    <option value="II">II</option>
                    <option value="III">III</option>
                    <option value="IV">IV</option>
                    <option value="V">V</option>
                  </Sel>
                </div>
                {/* Field Boks */}
                <div className="form-group row">
                  <Sel name="boks" label="Boks">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </Sel>
                </div>

                {/* Field Unggah Foto */}

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unggah File
                  </label>
                  <div className="col-lg-9 col-xl-9">
                    <Field
                      name="file"
                      component={CustomUploadDocument}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ArsipForm;
