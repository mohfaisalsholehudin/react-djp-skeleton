import React from 'react'
import SVG from "react-inlinesvg";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";
function ArsipOpen({
    history,
    match: {
      params: { id },
    },
  }) {
  return (
    <>
     <div className="d-flex flex-row">
          <div
            className="flex-row-auto offcanvas-mobile w-350px w-xxl-450px"
            id="kt_profile_aside"
          >
            <div className="card card-custom card-stretch">
              <div className="card-body pt-4">
                <div className="py-5">
                  <div className="d-flex align-items-center justify-content-between mb-9">
                    <h5 className="font-weight-bold">RINCIAN ARSIP</h5>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-9" >
                    <span className="font-weight-normal mr-10">Kode Pemberkasan:</span>
                    <span className="text-muted" style={{textAlign:'justify'}}>808080919191</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-9" >
                    <span className="font-weight-normal mr-10">Judul:</span>
                    <span className="text-muted" style={{textAlign:'justify'}}>SPT Tahunan | Budi Eko Prasetyo</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-9" >
                    <span className="font-weight-normal mr-10">
                      Tipe:
                    </span>
                    <span className="text-muted" style={{textAlign:'justify'}}>SPT Tahunan</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-9" >
                    <span className="font-weight-normal mr-10">Tahun:</span>
                    <span className="text-muted" style={{textAlign:'justify'}}>2019</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-9" >
                    <span className="font-weight-normal mr-10">Deskripsi:</span>
                    <span className="text-muted" style={{textAlign:'justify'}}>SPT Tahunan dari Wajib Pajak Budi Eko Prasetyo yang telah diarsipkan oleh salah satu petugas pajak di KPP Pratama Lubuk Linggau.</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-9" >
                    <span className="font-weight-normal mr-10">Jumlah Lembar:</span>
                    <span className="text-muted" style={{textAlign:'justify'}}>10</span>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mb-9" >
                    <span className="font-weight-normal mr-10">Lokasi Penyimpanan:</span>
                    <span className="text-muted" style={{textAlign:'justify'}}>E-3-IV-2</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex-row-fluid ml-lg-8">
            {/* <PembelianTambahTable /> */}
          </div>
        </div>
    </>
  )
}

export default ArsipOpen