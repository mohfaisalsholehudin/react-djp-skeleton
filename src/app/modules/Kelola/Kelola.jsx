import React,  { Suspense }  from "react";
import { useSelector } from "react-redux";
import {Redirect, Switch, Route } from "react-router-dom";
import {LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";
import Usulan from "./Usulan/Usulan";
import UsulanEdit from "./Usulan/UsulanEdit";
import Pinjam from "./Pinjam/Pinjam";
import Kembali from "./Kembali/Kembali";
import KembaliEdit from "./Kembali/KembaliEdit";
import UsulanRoutes from "./Usulan/UsulanRoutes";
import Kdo from "./Usulan/kdo/Kdo";
import Pemeliharaan from "./Pemeliharaan/Pemeliharaan";
import KdoEdit from "./Usulan/kdo/KdoEdit";
import PemeliharaanEdit from "./Pemeliharaan/PemeliharaanEdit";
import PemeliharaanRiwayat from "./Pemeliharaan/PemeliharaanRiwayat";


export default function Kelola() {

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
    <Switch>
          
        <Redirect
            exact={true}
            from="/kelola"
            to="/kelola/usulan"
        />


      {/* Begin Usulan */}
         <ContentRoute path="/kelola/usulan/pilih/arsip" component={UsulanEdit} />
         <ContentRoute path="/kelola/usulan/pilih/kdo/:id/pinjam" component={KdoEdit} />
         <ContentRoute path="/kelola/usulan/pilih/kdo" component={Kdo} />
         <ContentRoute path="/kelola/usulan/pilih" component={UsulanRoutes} />
         <ContentRoute path="/kelola/usulan" component={Usulan} />
      {/* End of Usulan */}

      {/* Begin Peminjaman */}
        <ContentRoute path="/kelola/peminjaman" component={Pinjam} />
      {/* End of Peminjaman */}

      {/* Begin Pengembalian */}
        <ContentRoute path="/kelola/pengembalian/:id/detail" component={KembaliEdit} />
        <ContentRoute path="/kelola/pengembalian" component={Kembali} />
      {/* End of Pengembalian */}

      {/* Begin Pemeliharaan */}
        <ContentRoute path="/kelola/pemeliharaan/:id/riwayat/tambah" component={PemeliharaanEdit} />
        <ContentRoute path="/kelola/pemeliharaan/:id/riwayat" component={PemeliharaanRiwayat} />
        <ContentRoute path="/kelola/pemeliharaan" component={Pemeliharaan} />
      {/* End of Pemeliharaan */}





    </Switch>
  </Suspense>

  );
}
