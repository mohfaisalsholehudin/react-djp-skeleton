import React, { useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../_metronic/_partials/controls";
import { Select as Sel } from "../../../helpers";

function UsulanRoutes() {
  const history = useHistory();

  const backAction = () => {
    history.push(`/kelola/usulan`);
  };

  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const initialValues = {
    jenis: "",
  };

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    jenis: Yup.string().required("Jenis Usulan wajib diisi"),
  });

  const saveForm = (val) => {
    switch (val.jenis) {
      case "Ruangan":
        history.push(`/kelola/usulan/pilih/ruangan`);
        break;
      case "KDO":
        history.push(`/kelola/usulan/pilih/kdo`);
        break;
      case "BMN":
        history.push(`/kelola/usulan/pilih/bmn`);
        break;
      case "Arsip":
        history.push(`/kelola/usulan/pilih/arsip`);
        break;

      default:
        break;
    }
  };
  return (
    <Card>
      <CardHeader
        title="Jenis Usulan Peminjaman"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={ProposalEditSchema}
            onSubmit={(values) => {
              saveForm(values);
            }}
          >
            {({ handleSubmit }) => {
              const jenis = [
                <option key={"Ruangan"} value={"Ruangan"}>
                  {"Ruangan"}
                </option>,
                <option key={"KDO"} value={"KDO"}>
                  {"Kendaraan Dinas Operasional"}
                </option>,
                <option key={"BMN"} value={"BMN"}>
                  {"Barang Milik Negara"}
                </option>,
                <option key={"Arsip"} value={"Arsip"}>
                  {"Arsip"}
                </option>,
              ];
              return (
                <>
                  <Form className="form form-label-right">
                    {/* Field Jenis */}
                    <div className="form-group row">
                      <Sel name="jenis" label="Jenis Usulan">
                        <option>--Pilih Jenis Peminjaman--</option>
                        {/* {status} */}
                        {jenis}
                      </Sel>
                    </div>

                    <button
                      type="submit"
                      style={{ display: "none" }}
                      ref={btnRef}
                      onSubmit={() => handleSubmit()}
                    ></button>
                    {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
                  </Form>
                </>
              );
            }}
          </Formik>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          <button
            type="submit"
            // onSubmit={() => handleSubmit()}
            onClick={saveButton}
            className="btn btn-success ml-2"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            {/* <i className="fas fa-check"></i> */}
            <i className="fa fa-arrow-right"></i>
            Selanjutnya
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default UsulanRoutes;
