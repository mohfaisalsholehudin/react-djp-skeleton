import React from "react";
import { useHistory } from "react-router-dom";

import KdoForm from "./KdoForm";

function KdoEdit() {
  const history = useHistory();



  const handleCancel = () => {
    history.push(`/kelola/usulan/pilih/kdo`);

  };
  return (
    <>
      <div className="d-flex flex-row">
        <div
          className="flex-row-auto offcanvas-mobile w-350px w-xxl-500px"
          id="kt_profile_aside"
        >
          <div className="card card-custom card-stretch">
            <div className="card-body pt-4">
              <div className="py-5">
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <h5 className="font-weight-bold">INFO PEMINJAMAN</h5>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <div>
                    <img
                      src="https://asap.kanwildjpriau.com/storage/fileupload/kdo/kdo_170193.jpg"
                      alt=""
                      style={{
                        width: "100%",
                      }}
                    />
                  </div>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <h4>Jenis:</h4>
                  <h4 className="text-muted">Triton</h4>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  {/* <span className="font-weight-normal mr-2">Tahun:</span>
                    <span className="text-muted">2016</span> */}
                  <h4>Tahun:</h4>
                  <h4 className="text-muted">2016</h4>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <h4>Nomor Plat:</h4>
                  <h4 className="text-muted">BM 9344 AP</h4>
                </div>
              </div>
            </div>
            {/* <div className="card-body pt-4">
              <div className="d-flex align-items-center justify-content-between mb-2">
                    <h5 className="font-weight-bold">MASUKKAN ITEM</h5>
                  </div>
              <PembelianTambahForm 
                id={content.id}
              />
              </div> */}
          </div>
        </div>
        <div className="flex-row-fluid ml-lg-8">
          <div className="card card-custom card-stretch">
            {/* begin::Form */}
            <div className="form">
              {/* begin::Body */}
              <div className="card-body" style={{ paddingTop: "0px" }}>
                <div className="row" style={{ display: "block" }}>
                  <label className="col-xl-3"></label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ paddingLeft: "0px" }}
                  >
                    <h5 className="font-weight-bold mb-6">DATA PEMINJAMAN</h5>
                  </div>
                </div>
                {/* <div className="row"> */}
                  <>
                  <KdoForm />
                  </>
                {/* </div> */}
                <div
                  className="row mt-3"
                  style={{ display: "block", textAlign: "right" }}
                >
                  <button
                    type="submit"
                    className="btn btn-danger"
                      onClick={() => handleCancel()}
                    disabled={false}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      // paddingRight: "0px", paddingLeft: "0px"
                    }}
                    // disabled={disabled}
                  >
                    <i className="far fa-times-circle"></i>
                    Batalkan
                  </button>
                  <button
                    type="submit"
                    className="btn btn-success ml-2"
                    // onClick={() => handleSavePurchaseAtk()}
                    disabled={false}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      // paddingRight: "0px", paddingLeft: "0px"
                    }}
                    // disabled={disabled}
                  >
                    <i className="fas fa-save"></i>
                    Simpan
                  </button>
                </div>
              </div>
              {/* end::Body */}
            </div>
            {/* end::Form */}
          </div>
        </div>
      </div>
    </>
  );
}

export default KdoEdit;
