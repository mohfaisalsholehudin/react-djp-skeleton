/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import SVG from "react-inlinesvg";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import UsulanTable from "./UsulanTable";


function Usulan() {
  const history = useHistory();
  const [tab, setTab] = useState("proses");


  const add = () => history.push("/kelola/usulan/pilih");

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Usulan Peminjaman Aset"
          style={{ backgroundColor: "#FFC91B" }}
        >
          <CardHeaderToolbar>
            <button
              type="button"
              className="btn btn-primary ml-2"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                float: "right",
              }}
              onClick={add}
            >
              <span className="svg-icon menu-icon">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} />
              </span>
              Tambah
            </button>
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          {/* <UsulanTable /> */}
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("proses")}>
              <a
                className={`nav-link ${tab === "proses" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "proses").toString()}
              >
                Proses
              </a>
            </li>
            {/* {id && ( */}
            <>
              {" "}
              <li className="nav-item" onClick={() => setTab("monitoring")}>
                <a
                  className={`nav-link ${tab === "monitoring" && "active"}`}
                  data-toggle="tab"
                  role="button"
                  aria-selected={(tab === "monitoring").toString()}
                >
                  Monitoring
                </a>
              </li>
            </>
            {/* )} */}
          </ul>
          <div className="mt-5">
            {tab === "proses" && <UsulanTable />}
            {tab === "monitoring" && <UsulanTable tab={"monitoring"} />}
          </div>
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Usulan;
