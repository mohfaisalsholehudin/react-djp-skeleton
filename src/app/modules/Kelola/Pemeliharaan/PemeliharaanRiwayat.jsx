import React from "react";
import PemeliharaanRiwayatTable from "./PemeliharaanRiwayatTable";
import { IdColumnFormatter } from "../../../helpers/column-formatters";
import { idea } from "react-syntax-highlighter/dist/cjs/styles/hljs";

function PemeliharaanRiwayat({
  history,
  match: {
    params: { id },
  },
}) {
  const handleBack = () => {
    history.push(`/kelola/pemeliharaan`);
  };
  return (
    <>
      <div className="d-flex flex-row">
        <div
          className="flex-row-auto offcanvas-mobile w-350px w-xxl-500px"
          id="kt_profile_aside"
        >
          <div className="card card-custom card-stretch">
            <div className="card-body pt-4">
              {/* <div className="py-5"> */}
              <div className="row" style={{ display: "block" }}>
                <div
                  className="col-lg-9 col-xl-6"
                  style={{ paddingLeft: "0px" }}
                >
                  <h5 className="font-weight-bold mb-6">Info KDO</h5>
                </div>
              </div>
              {/* <div className="d-flex align-items-center justify-content-between mb-2">
                  <h5 className="font-weight-bold">Detail Kendaraan Dinas Operasional</h5>
                </div> */}
              <div className="d-flex align-items-center justify-content-between mb-4">
                <div>
                  <img
                    src="https://asap.kanwildjpriau.com/storage/fileupload/kdo/kdo_170193.jpg"
                    alt=""
                    style={{
                      width: "100%",
                    }}
                  />
                </div>
              </div>
              <div className="d-flex align-items-center justify-content-between mb-2">
                <h4>Jenis:</h4>
                <h4 className="text-muted">Triton</h4>
              </div>
              <div className="d-flex align-items-center justify-content-between mb-2">
                {/* <span className="font-weight-normal mr-2">Tahun:</span>
                  <span className="text-muted">2016</span> */}
                <h4>Tahun:</h4>
                <h4 className="text-muted">2016</h4>
              </div>
              <div className="d-flex align-items-center justify-content-between mb-2">
                <h4>Nomor Plat:</h4>
                <h4 className="text-muted">BM 9344 AP</h4>
              </div>
              {/* </div> py-5 */}
            </div>
            {/* <div className="card-body pt-4">
            <div className="d-flex align-items-center justify-content-between mb-2">
                  <h5 className="font-weight-bold">MASUKKAN ITEM</h5>
                </div>
            <PembelianTambahForm 
              id={content.id}
            />
            </div> */}
          </div>
        </div>
        <div className="flex-row-fluid ml-lg-8">
          <div className="card card-custom card-stretch">
            {/* begin::Form */}
            <div className="form">
              {/* begin::Body */}
              <div className="card-body" style={{ paddingTop: "0px" }}>
                <div className="row" style={{ display: "block" }}>
                  <label className="col-xl-3"></label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ paddingLeft: "0px" }}
                  >
                    <h5 className="font-weight-bold mb-6">Riwayat Perawatan</h5>
                  </div>
                </div>
                {/* <div className="row"> */}
                <>
                  <PemeliharaanRiwayatTable id={id} />
                </>
                <div
                  className="row mt-3"
                  style={{ display: "block", textAlign: "right" }}
                >
                  <button
                    type="button"
                    className="btn btn-light"
                    onClick={() => handleBack()}
                    disabled={false}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      // paddingRight: "0px", paddingLeft: "0px"
                    }}
                    // disabled={disabled}
                  >
                    <i className="fa fa-arrow-left"></i>
                    Kembali
                  </button>
                </div>
                {/* </div> */}
              </div>
              {/* end::Body */}
            </div>
            {/* end::Form */}
          </div>
        </div>
      </div>
    </>
  );
}

export default PemeliharaanRiwayat;
