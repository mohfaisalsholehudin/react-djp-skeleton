import axios from "axios";
// const {BACKEND_URL} = window.ENV;

// const config = {
//   headers: {
//     "Content-type": "application/x-www-form-urlencoded",
//     Authorization: `Basic ${pass}`,
//   },
// };
export const LOGIN_URL = "/api/v1/auth/login";
export const ME_URL = "/api/v1/auth/whoami";


export const REGISTER_URL = "api/auth/register";
export const REQUEST_PASSWORD_URL = "api/auth/forgot-password";


// export function login(username, password) {
//   return axios.get(LOGIN_URL);
// }
export function login(username, password) {
  return axios.post(LOGIN_URL, { username, password });
}

export function register(email, fullname, username, password) {
  return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

// export function getUserByToken() {
//   // Authorization head should be fulfilled in interceptor.
//   return axios.get(ME_URL);
// }
// export function getUserByToken(token) {

//   const options ={ headers: { 'Authorization' : 'Bearer ' + token, "Content-Type": "application/json"}}
//   // Authorization head should be fulfilled in interceptor.
//   return axios.post(ME_URL,"",options);
// }

export function getUserByToken(token) {
  return axios.post(ME_URL);
}